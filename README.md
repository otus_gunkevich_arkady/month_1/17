# 17 C# Developer. Professional

 - Работа с методами как с переменными (delegates, events) // ДЗ
 - Гункевич А.И.

## Основные понятия, цели, размышления

 - Функция расширения, она же "метод расширения" или если быть как *аммэрикан пипл* "extension methods", очень удобная штука, но крайне специфичная. На опыте никогда не применял, но подозревал что в C# есть такое.
 - Делегаты, события, очень нужный механизм, его использование безгранично. 

## Про решение

 - Мне хочется выстраивать архитектуру приложения которое пишу ещё до его написания, но почему в голове не получается составить её. Я слышал есть метод проектирования TDD, очень похоже на что я делаю.
 - Добавил несколько методов расширения дополнительно.
 - Хосподь всемогущий, да нам нужно объект превратить во float (поздно заметил "where T : class;"), но слава богам string подходит под условие. UPD | Кажется 6 задание испортило мой изящный алгоритм преобразования во float

## Итоги

 - Задание не вдохновило =((( Надеюсь оно сделано.
 - С момента последней работы, с момента последней программы для OTUS прошло больше месяца (сегодня 05.07.2023) И снова "Здравствуйте"