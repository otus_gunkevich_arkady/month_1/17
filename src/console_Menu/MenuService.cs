﻿
using console_Menu.Component;

namespace console_Menu
{
    public class MenuService : IMenuService
    {
        protected Menu _menu;
        protected List<MenuButton> _buttons = new List<MenuButton>();
        protected List<string> _text = new List<string>();
        protected bool _isBlock = false;

        protected bool _exit = false;
        public virtual void Enter() => _menu.ExecuteButton();
        public virtual void Up() => _menu.UpSelect();
        public virtual void Down() => _menu.DownSelect();
        public virtual void Left() { }
        public virtual void Right() { }
        public virtual void Write()
        {
            UpdateMenu();
            Console.Clear();
            _menu.Buttons.ForEach(s =>
            {
                if (s.IsSelect) Console.BackgroundColor = s.ColorSelect;
                Console.WriteLine($"{s.Text}");
                Console.BackgroundColor = ConsoleColor.Black;
            });
            Console.WriteLine("====================================");
            _menu.Text.ForEach(s => Console.WriteLine(s));
        }
        public virtual bool Exit() => _exit;
        public virtual void UpdateMenu() => _menu = new Menu(_buttons, _text);
        protected virtual void Log(string text) => _text.Add(text);

        public bool IsBlock() => _isBlock;
    }

}

