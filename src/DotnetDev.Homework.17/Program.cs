﻿using console_Menu;
using DotnetDev.Homework._17;

public class Program
{
    private static void Main(string[] args) => new MenuVisual(new Start()).Visual();
}