﻿using console_Menu;
using console_Menu.Component;

namespace DotnetDev.Homework._17
{
    public class Start : MenuService
    {
        public Start()
        {
            _buttons = new List<MenuButton>
            {
                new MenuButton("Вывести ДЗ!", () => FileGetAround(_pathFileFind)),
                new MenuButton("(не обязательно) Функции расширения!", () => new MenuVisual(new MethodExtensionDemonstration()).Visual()),
                new MenuButton("[*] Выйти", () => _exit = true)
            };

            _text = new List<string>
            {
                "",
                "",
                "Гункевич Аркадий Игоревич | OTUS",
                "ДЗ: Работа с методами как с переменными (delegates, events)"
            };
        }

        private string _pathFileFind = "C:\\Windows";
        private List<string> _listFiles;
        private void FileGetAround(string path)
        {
            FileAround _fileAround = new FileAround(path);
            _listFiles = new List<string>();
            _fileAround.HandleFindFile += FileChecker;
            _fileAround.AroundEnd += FileMax;
            //Заблокируем меню, пока не закончим обход файлов
            _isBlock = true;
            //Задержка для наглядности (Task.Delay(0.2)) 
            _fileAround.GetAround();
        }
        private bool? FileChecker(object sender, FileArgs e)
        {
            _text = new List<string>();
            _listFiles.Add(e.GetFileInfo().Name);
            _listFiles.WriteAllElementIsStringNewLine<string>(Log);
            if (_listFiles.Count > 20)
            {
                FileMax();
                return true;
            }
            else
            {
                Write();
                return false;
            }
        }
        private void FileMax()
        {
            Log($"{_listFiles.Count} файлов" +
                $"\nМаксимальный - {_listFiles.GetMax<string>(Util.ConvertationFloat)}" +
                $"\nМинимальный - {_listFiles.GetMin<string>(Util.ConvertationFloat)}");
            Write();
            _isBlock = false;
        }
    }
}
