﻿namespace DotnetDev.Homework._17
{
    public class FileAround
    {
        public delegate bool? EventHandler(object sender, FileArgs e);
        public event EventHandler HandleFindFile;
        public event Action AroundEnd;
        private string _path;
        public FileAround(string path)
        {
            _path = path;
        }
        public async void GetAround()
        {
            foreach (FileInfo _file in Directory.GetFiles(_path).Select(s => new FileInfo(s)).ToList())
            {
                //Подождём 1 секунду
                if (HandleFindFile?.Invoke(this, new FileArgs(_file)) == true)
                    return;
                await Task.Delay(TimeSpan.FromSeconds(0.2));
            }
            AroundEnd?.Invoke();
        }
    }
}
