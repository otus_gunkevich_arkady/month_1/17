﻿namespace DotnetDev.Homework._17
{
    public class FileArgs : EventArgs
    {
        private FileInfo _file;
        public FileArgs(FileInfo file)
        {
            _file = file;
        }
        public FileInfo GetFileInfo() => _file;
    }
}
