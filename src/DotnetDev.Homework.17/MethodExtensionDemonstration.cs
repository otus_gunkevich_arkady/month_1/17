﻿using console_Menu;
using console_Menu.Component;

namespace DotnetDev.Homework._17
{
    /// <summary>
    /// Просто демонстрация того как вроде как должно работать
    /// </summary>
    public class MethodExtensionDemonstration : MenuService
    {
        //Венгерская нотация - поехали
        private readonly string[] saDays = { "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресень" };
        public MethodExtensionDemonstration()
        {
            _buttons = new List<MenuButton>
            {
                new MenuButton("WriteAllElementIsString<T>", () => WriteAll()),
                new MenuButton("GetMax<T>", () => GetMax()),
                new MenuButton("GetMin<T>", () => GetMin()),
                new MenuButton("[*] Назад", () => _exit = true)
            };
        }
        private void WriteAll()
        {
            _text = new List<string>();
            //Через Log(string)
            Log($"{nameof(saDays)} - {saDays.GetType()}");
            saDays.WriteAllElementIsStringNewLine<string>(Log);
        }
        private void GetMax()
        {
            _text = new List<string>()
            {
                $"{nameof(saDays)} - {saDays.GetType()}",
                saDays.GetAllElementIsString<string>(),
                $"Максимальное: {saDays.GetMax<string>(Util.ConvertationFloat)}"
            };
        }
        private void GetMin()
        {
            _text = new List<string>()
            {
                $"{nameof(saDays)} - {saDays.GetType()}",
                saDays.GetAllElementIsString<string>(),
                $"Минимальное: {saDays.GetMin<string>(Util.ConvertationFloat)}"
            };
        }
    }
}
