﻿using System.Collections;

namespace DotnetDev.Homework._17
{
    /// <summary>
    /// Класс, с методами расширения
    /// </summary>
    public static class MethodExtension
    {
        /// <summary>
        /// Все элементы в строку (Action<string>)
        /// </summary>
        public static void WriteAllElementIsString<T>(this IEnumerable e, Action<string> log) where T : class
        {
            string s = string.Empty;
            foreach (T item in e)
            {
                s += item?.ToString() + " ";
            }
            log(s);
        }
        /// <summary>
        /// Все элемент через строку (Action<string>)
        /// </summary>
        public static void WriteAllElementIsStringNewLine<T>(this IEnumerable e, Action<string> log) where T : class
        {
            foreach (T item in e)
            {
                log(item?.ToString());
            }
        }
        /// <summary>
        /// Вернуть все элементы строкой
        /// </summary>
        public static string GetAllElementIsString<T>(this IEnumerable e) where T : class
        {
            string s = string.Empty;
            foreach (T item in e)
            {
                s += item.ToString() + " ";
            }
            return s;
        }
        /// <summary>
        /// Найти максимальный элемент
        /// </summary>
        public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            T _retValue = null;
            foreach (T item in e)
            {
                if (_retValue is null) _retValue = item;
                if (getParameter(item) > getParameter(_retValue)) _retValue = item;
            }
            return _retValue;
        }
        /// <summary>
        /// Найти минимальный элемент
        /// </summary>
        public static T GetMin<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            T _retValue = null;
            foreach (T item in e)
            {
                if (_retValue is null) _retValue = item;
                if (getParameter(item) < getParameter(_retValue)) _retValue = item;
            }
            return _retValue;
        }
    }
}
