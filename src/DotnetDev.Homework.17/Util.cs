﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotnetDev.Homework._17
{
    /// <summary>
    /// С названиями беда. Класс в котором я соберу нужные функции.
    /// </summary>
    public static class Util
    {
        public static float ConvertationFloat(object T)
        {
            ///Прошлый изящный алгоритм перобразования во FLOAT
            //try
            //{
            //    return Convert.ToSingle(T);
            //}
            //catch
            //{
            //    return 0f;
            //}


            ///Ради 6-го задания
            ///Новый изящный алгоритм перобразования во FLOAT
            try
            {
                //Я не особо знаю что придумать, самый простой способ количество символов в строке
                if (T.GetType() == typeof(string)) return T.ToString().Length;

                return Convert.ToSingle(T);
            }
            catch
            {
                return 0f;
            }
        }
    }
}
